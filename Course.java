package ir.ac.kntu;

import java.util.HashMap;
import java.util.Map;
import java.util.ArrayList;

public class Course {
	private String name;
	private String lecturer;
	private Date3 startDate;
	private ArrayList<Student> register;
	private Map<Student, Double> marks;
	
	public Course(String name, String lecturer, Date3 startDate) {
		this.name = name;
		this.lecturer = lecturer;
		this.startDate = startDate;
		register = new ArrayList<Student>();
		marks = new HashMap<Student, Double>();
	}
	
	public String getName() {
		return name;
	}
	
	public boolean register(Student student) {
		if (!register.contains(student)) {
			register.add(student);
			return true;
		}
		return false;
	}
	
	public boolean registerMark(Student student, Double mark) {
		if (register.contains(student)) {
			marks.put(student, mark);
			return true;
		} 
		return false;
	}
	
	public String toString() {
		return "Course: " + name + ", Lecturer: " + lecturer + "\r\n";
	}
	
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((lecturer == null) ? 0 : lecturer.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result
				+ ((startDate == null) ? 0 : startDate.hashCode());
		return result;
	}

	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Course other = (Course) obj;
		if (lecturer == null) {
			if (other.lecturer != null)
				return false;
		} else if (!lecturer.equals(other.lecturer))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (startDate == null) {
			if (other.startDate != null)
				return false;
		} else if (!startDate.equals(other.startDate))
			return false;
		return true;
	}
}

	