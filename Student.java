package ir.ac.kntu;

public class Student {
	private String name;
	private String familyName;
	private Date3 birthDate;
	private String id;
	private Date3 entryDate;

	public Student(String id, String name, String familyName, Date3 entryDate, Date3 birthDate) {
		super(name, familyName, birthdate);
		this.id = id;
		this.entryDate = entryDate;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFamilyName() {
		return familyName;
	}

	public Date3 getBirthDate() {
		return birthDate;
	}

	public String getId() {
		return id;
	}

	public String toString() {
		return "Student id: " + id + "\r\n" + "Full Name: " + name + " " + familyName + "\r\n" +
				"Entry Date: " + entryDate + "\r\n" + "Birth Date: " + birthDate + "\r\n";
	}

	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Student other = (Student) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}