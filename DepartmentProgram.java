package ir.ac.kntu;

import java.util.Scanner;

public class DepartmentProgram {
	public static void main(String[] argv) {
		int option;
		Department department = null;
		Scanner scanner = new Scanner(System.in);
		printTheMenu();

		option = scanner.nextInt();
		while (option != 6) {
			department = handleTheOption(scanner, department, option);
			printTheMenu();
			option = scanner.nextInt();
		} 
		scanner.close();
	}
	
	public static Department handleTheOption(Scanner scanner, Department department, int option) {
		switch (option) {
			case 1:
				if (department == null) {
					department = defineDepartment(scanner);
				} else {
					System.out.println("You've already defined the department!");
				}
				break;
			case 2:
				addCourse(scanner, department);
				break;
			case 3:
				addStudent(scanner, department);
				break;
			case 4:
				registerToACourse(scanner, department);
				break;
			case 5:
				registerAMark(scanner, department);
				break;
			default:
				System.out.println("Invalid choice!");
				break;
		}
		return department;
	}
	
	public static void registerAMark(Scanner scanner, Department department) {
		Student student;
		Course course;
		Double mark;
		if (department != null) {
			student = getStudent(scanner, department);
			if (student != null) {
				course = getCourse(scanner, department);
				if (course != null) {
					System.out.println("Enter the mark: ");
					mark = scanner.nextDouble();
					if (course.registerMark(student, mark)) {
						System.out.println("Mark is successfully registered.");
					} else {
						System.out.println("This student is not known!");
					}
				} else {
					System.out.println("This course is not defined!");
				}
			} else {
				System.out.println("This student is not known!");
			}
		} else {
			System.out.println("You have to define a department first!");
		}
	}

	public static void registerToACourse(Scanner scanner, Department department) {
		Student student;
		Course course;
		if (department != null) {
			student = getStudent(scanner, department);
			if (student != null) {
				course = getCourse(scanner, department);
				if (course != null) {
					if (course.register(student)) {
						System.out.println("Successfully registered.");
					} else {
						System.out.println("Already registered!");
					}
				} else {
					System.out.println("This course is not defined!");
				}
			} else {
				System.out.println("This student is not known!");
			}
		} else {
			System.out.println("You have to define a department first!");
		}
	}

	public static Student getStudent(Scanner scanner, Department department) {
		String id;
		System.out.println("Enter the student id: ");
		id = scanner.next();
		return department.getStudent(id);
	}
	
	public static Course getCourse(Scanner scanner, Department department) {
		String courseName;
		System.out.println("Enter the course name: ");
		courseName = scanner.next();
		return department.getCourse(courseName);
	}
	
	public static Department defineDepartment(Scanner scanner) {
		Department department;
		String name;
		System.out.print("Enter the department name: ");
		name = scanner.next();
		department = new Department(name);
		System.out.println("Successfully defined.");
		return department;
	}
	
	public static void addCourse(Scanner scanner, Department department) {
		if (department != null) {
			String name, lecturer;
			Date3 date;
			System.out.print("Enter the course name: ");
			name = scanner.next();
			System.out.print("Enter the lecturer name: ");
			lecturer = scanner.next();
			date = getDate(scanner, "start");
			Course course = new Course(name, lecturer, date);
			if (department.addCourse(course)) {
				System.out.println("Successfully added.");
			} else {
				System.out.println("Already exists");
			}
		} else {
			System.out.println("You have to define a department first!");
		}
	}
	
	public static void addStudent(Scanner scanner, Department department) {
		if (department != null) {
			String id, name, familyName;
			Date3 entryDate, birthDate;
			System.out.print("Enter the student id: ");
			id = scanner.next();
			System.out.print("Enter the student name: ");
			name = scanner.next();
			System.out.print("Enter the student family name: ");
			familyName = scanner.next();
			entryDate = getDate(scanner, "entry");
			birthDate = getDate(scanner, "birth");
			Student student = new Student(id, name, familyName, entryDate, birthDate);
			if (department.addStudent(student)) {
				System.out.println("Successfully added.");
			} else {
				System.out.println("Already exists");
			}
		} else {
			System.out.println("You have to define a department first!");
		}
	}
	
	public static Date3 getDate(Scanner scanner, String dateName) {
		int year, month, day;
		System.out.print("Enter the " + dateName + " year: ");
		year = scanner.nextInt();
		System.out.print("Enter the " + dateName + " month: ");
		month = scanner.nextInt();
		System.out.print("Enter the " + dateName + " day: ");
		day = scanner.nextInt();
		return new Date3(year, month, day);
	}
	
	public static void printTheMenu() {
		System.out.println("***********************************");
		System.out.println("Department options:");
		System.out.println("1-Define the department.");
		System.out.println("2-Add a course.");
		System.out.println("3-Add a student.");
		System.out.println("4-Register to a course.");
		System.out.println("5-Register a mark.");
		System.out.println("6-Exit.");
		System.out.println("***********************************");
		System.out.print("\r\nPlease select your choice: ");
	}
}