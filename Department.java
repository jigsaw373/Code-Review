package ir.ac.kntu;

import java.util.ArrayList;

public class Department {
	private String name;
	private ArrayList<Course> courses;
	private ArrayList<Student> students;
	
	public Department(String name) {
		this.name = name;
		courses = new ArrayList<Course>();
		students = new ArrayList<Student>();
	}
	
	public boolean addCourse(Course course) {
		if (!courses.contains(course)) {
			courses.add(course);
			return true;
		}
		return false;
	}
	
	public boolean addStudent(Student student) {
		if (!students.contains(student)) {
			students.add(student);
			return true;
		}
		return false;
	}

	public Student getStudent(String studentId) {
		for (Student student : students) {
			if (student.getId().equals(studentId)) {
				return student;
			}
		}
		return null;
	}
	
	public Course getCourse(String courseName) {
		for (Course course : courses) {
			if (course.getName().equals(courseName)) {
				return course;
			}
		}
		return null;
	}
	
	public String toString() {
		return "Department: " + name + "\r\n";
	}	
}
